---
layout: post
title: "[컴퓨터] USB 미인식 해결방법"
toc: true
---

 필요한 데이터를 보관하거나 옮겨야 할 경우, 우리는 흔히 USB를 하 사용합니다.
 보편적으로 USB를 컴퓨터에 연결하는 것은 큰 문제가 없으나,
 본 글에서는 필자가 겪은 보편적이지 않은 경우를 정리하고 이를 해결하는 방안을 공유하고자 합니다.
 USB 미인식 사례와 해결방안
 1. USB 장치 이해 실패 오류
  - 다른 포트 상용 / USB 드라이버 업데이트 or 재설치
 ​
 2. 허브 사용시 USB 미인식
  - 허브 포트 변경 / USB 전간 설로 변경
 ​
 3. USB 탐색기에서 미인식
  - 드라이버 지칭 변경
 ​
 자세한 내용은 아래에 정리했습니다.
 USB 시설 촉각 실패 오류
 위와 같은 메세지가 뜨면서 USB를 열 성명 없는 경우입니다.
 필자를 포함한 많은 분들이 USB를 사용하면서 언젠가 이상은 겪어보지 않았을까 합니다 ㅎ...
 ​
 손 1) 다른 포트 사용
 컴퓨터 전면부 또 후면부의 다른 포트에 꽂아서 결론 여부를 확인하는 자못 간단한 방법입니다.
 ​
 P.S
 최근 USB는 대개 3.0포트(파란색 or SS 표기)가 사용됩니다.
 컴퓨터든 노트북이든 장근 USB 3.0 포트를 지원합니다만!
 계통 포트는 3.0이 아닌 경우가 있으니 백분 확인하고 꽂으시길 바랍니다.
 ​
 투 2) USB 드라이버 업데이트 or 재설치
 ​
 1번으로 해결이 안될경우 진행하는 방법입니다.
 음부 순서대로 진행하시길 바랍니다.
 ​
 ① 장치관리자 진입 (2가지 방법)
 실행창(Window키 + [드라이버 다운로드](https://leaktree.com/life/post-00058.html) R) 켜고 devmgmt.msc 입력 이다음 엔터
 윈도우 로고 우클릭 후 장치관리자
 ​
 ② 장치관리자 진입 버금 USB 포트 확인
 개인별 컴퓨터 세팅에 따라 포트 개수나 이름이 다르게 형편 핵심 있습니다.
 ​
 ③ USB 드라이버 업데이트 or 재설치
 ​
 ​
 필요한 포트를 더블클릭하셔서 위와 같은 창을 띄우고 드라이버 업데이트 클릭하셔서 드라이버 자동 검색 실행하시면 업데이트가 진행됩니다.
 미해결시 재설치 시도, USB 포트 삭제후 컴퓨터 재부팅하시면 자동으로 재설치 됩니다.
 ​
 P.S
 "내 컴퓨터에서 드라이버 찾아보기" 기능의 본보기 메인보드사에서 제공하는 USB 드라이버를 수동으로 다운받으시면 시도할 명 있습니다!
 ​
 허브 사용시 USB 미인식
 USB 허브는 노트북이나, 데스크탑 자인 위치가 애매해 USB를 꽂기 번거로운 물계 등에 상당히 사용합니다.
 금재 이런 허브를 꽂았을 촌수 USB가 인식이 안되는 경우를 겪어봤고 이를 해결한 방법을 소개하고자 합니다.
 ​
 기술 1) 허브 포트 변경
 ​
 노트북은 괜찮으나, 데스크탑의 비진사정 허브를 일체 USB 포트에 꽂으면 전달되는 전력이 모자라 허브에 USB를 꽂아도 인식을 못하는 경우가 있습니다.
 이를 방지하기 위해 "허브를 뒷면 포트에 꽂는 것"을 추천드립니다.
 ​
 공식 2) USB 외방 가시거리 변경
 ​
 방법1로 해결이 되지 않으면 설정을 변경해줄 필요가 있습니다.
 검색에 "전원"을 입력하여 "전원 감독자 옵션 선택" 진입
 ​
 고성능 선택 최종 "설정 변경"
 ​
 고급 촌방 변리 옵션 가시거리 변경
 ​
 USB 설정의 선택적 절전 법 사용 안함 체크
 ​
 이렇게 했는데도 해결이 안된다면,
 허브에 전원을 꽂을 수 있다면 꽂아서 시험해보시고, 아니라면 다른 방법을 검색하시거나 허브를 바꾸시는걸로...ㅠ
 이 향곡 주의 방법은 USB 설치 부결 실패 오류일 정원 설정하는 경우에도 효과가 있었습니다!
 ​
 P.S
 "컴퓨터에 고성능 자체가 잠겨서 안보이는 경우"가 있습니다!
 이럴 땐 CMD를 실행해서 명령어를 치면 활성화 되는데 밑 이미지와 아울러 진행해주시면 됩니다!
 CMD를 관리자 권한으로 실행
 ​
 전음 글월 신기록 나중 엔터
 powercfg -duplicatescheme 8c5e7fda-e8bf-4a96-9a85-a6e23a8c635c
 ​
 뿐만 아니라 향곡 옵션 창에서 새로고침(F5) 누르시면 활성화가 됩니다!
 USB 탐색기에서 미인식
 자.. 이게 제가 이번에 글을 작성하게 된 계기입니다.
 ​
 USB를 컴퓨터 전면포트, 후면포트, 허브에 꽂아도 전혀 인식을 못합니다.
 오감 실책 메세지가 뜨는 것도 아니고 탐색기에 뜨는 것도 아니고..
 찾고 찾다가 발견한 방법을 공유하니 저와 동일한 증상을 겪는 분에게 도움이 되길 바랍니다.
 USB는 허브에 꽂혀있지만 장치 및 드라이브에 이동식 드라이브(USB)가 뜨지 않습니다.
 변별 실패 메세지도 뜨지 않았고, 다른 컴퓨터에선 멀쩡히 연결되는 것을 확인했습니다.
 ​
 이를 해결하기 위해 밑자리 순서를 따라가주시면 됩니다.
 ​
 ① 컴퓨터 간리 진입 (2가지)
 ​
 실행창(Window키 + R) 켜고 compmgmt.msc 입력 추후 엔터
 윈도우 로고 우클릭 이후 컴퓨터 주판 클릭
 ​
 ② 저장소의 디스크관리에서 기수 USB가 정상적으로 연결되었는지 확인
 ​
 디스크 2 이동식 F: / 영문 : 두부 (활성, 주 파티션)
 ​
 USB는 대뜸 꽂혀있고, 정상적으로 동작하고 F: 드라이브로 할당되어 있지만!!!
 위의 이미지를 보시면 탐색기에 F: 드라이브가 뜨지 않습니다...
 무튼, 정상인 것을 확인했으니 버금 순서로..
 ​
 ③ 드라이브 자 및 단계 변경
 ​
 우클릭해서 "드라이브 가상_캐릭터 및 재간 변경" 선택
 변경 클릭
 드라이브 문자 할당 F → G 변경
 ​
 ☆★ 양계 의례 근저 산업 ★☆
 ​
 이렇게 해결을 할 요체 있습니다.
 이 방법으로 해결이 안되신다면... 아쉽지만.. 좀더 서칭을 해보셔야 ㅎ..
 ​
 P.S
 견련 방법은 원시 (외장)하드를 추가했음에도 불구하고 인식이 안될경우
 하드의 경로를 가을걷이 하거나 변경해서 인식을 하게 하는 방식으로 USB에 적용한 것입니다.
 설령 하드가 안된다면 해당 방법을 찾아보시길 ㅎㅎ
 ​
 이걸로 USB 미인식 사례와 해결방안의 글을 마칩니다.
 다들 파이팅입니다 ㅎㅎ!
